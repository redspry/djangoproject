from django.test import TestCase
# from unittest import TestCase
from PokerProjectDjango.PokerProject.classes import dealer


class DealerTest(TestCase):

    def setUp(self):
        # creating testing admin
        self.admin1 = dealer.PokerAdmin("one", "pass")  # creating test admins to be tested
        self.admin2 = dealer.PokerAdmin("two", "password")
        self.admin3 = dealer.PokerAdmin("three", "passwords")

    def test_constructor(self):
        with self.assertRaises(TypeError):
            dealer.PokerAdmin(False, "yup")  # testing errors with constructor
        with self.assertRaises(TypeError):
            dealer.PokerAdmin("yup", True)
        with self.assertRaises(TypeError):
            dealer.PokerAdmin(1, 2)
        with self.assertRaises(TypeError):
            dealer.PokerAdmin(2, False)

    def test_check_username(self):
        self.assertTrue(self.admin1.check_username("one"))                          #testing username is set and checked correctly
        self.assertTrue(self.admin2.check_username("two"))
        self.assertTrue(self.admin3.check_username("three"))
        self.assertFalse(self.admin1.check_username("two"))                         #testing username check with incorrect check
        self.assertFalse(self.admin2.check_username("three"))
        self.assertFalse(self.admin3.check_username("one"))
        with self.assertRaises(TypeError):
            self.admin1.check_username(False)                                        #testing check username with invalid type. Should return TypeError
        with self.assertRaises(TypeError):
            self.admin2.check_username(True)
        with self.assertRaises(TypeError):
            self.admin3.check_username(1)

    def test_reset_password(self):
        with self.assertRaises(TypeError):
            self.admin1.reset_password(False)                                       #testing reset password with new passwords and an incorrect parameter
        with self.assertRaises(TypeError):
            self.admin2.reset_password(1)
        self.admin1.reset_password("ONE")                                           #resetting the passwords and checking they are set correctly
        self.assertEqual(self.admin1.password, "ONE")
        self.assertNotEqual(self.admin1.password, "one")
        self.admin2.reset_password("TWO")
        self.assertEqual(self.admin2.password, "TWO")

    def test_create_new_player(self):
        self.admin1.create_new_player("player", "p1pass")                           #testing create new player
        self.admin2.create_new_player("player2", "p2pass")
        with self.assertRaises(TypeError):
            self.admin2.create_new_player(False, 2)                                 #checking that invalid creation of player returns a TypeError
        with self.assertRaises(TypeError):
            self.admin3.create_new_player(2, "yes")
        with self.assertRaises(TypeError):
            self.admin1.create_new_player("yes", True)
