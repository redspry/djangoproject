from django.test import TestCase
# from unittest import TestCase
from PokerProjectDjango.PokerProject.classes import card
from PokerProjectDjango.PokerProject.classes import deck


class DeckInit(TestCase):
    # Creates a valid deck for testing
    def setUp(self):
        self.actual_response = deck.Deck()
        self.validDeck = []
        for currentSuit in range(1, 5):  # valid deck
            for currentRank in range(1, 14):
                card1 = card.Card(currentSuit, currentRank)
                self.validDeck.append(card1)

    # Test deck initialization with default constructor
    def test_DeckInit(self):
        self.assertEqual(self.validDeck, self.actual_response.validDeck)


class DeckShuffle(TestCase):
    # Test deck shuffle method for Deck class
    # setup valid deck with card objects
    def setUp(self):
        self.actual_response1 = deck.Deck()  # Create and initialize a Deck
        self.validDeck = []
        for currentSuit in range(1, 5):  # valid deck
            for currentRank in range(1, 14):
                card1 = card.Card(currentSuit, currentRank)
                self.validDeck.append(card1)

    # test shuffle method of deck class
    # if shuffle method works, the decks should not be equal
    def test_correct_shuffle(self):
        actual_response2 = self.actual_response1.shuffle()  # shuffle second deck
        self.assertNotEqual(self.validDeck, actual_response2.validDeck)  # check that decks are not equal


class DeckCount(TestCase):
    # setup valid deck with card objects
    def setUp(self):
        self.actual_response = deck.Deck()  # create fake deck until deck.py is finished
        self.actual_response1 = deck.Deck()  # create fake deck until deck.py is finished
        self.actual_response2 = deck.Deck()  # create fake deck until deck.py is finished
        self.actual_response3 = deck.Deck()  # create fake deck until deck.py is finished

        self.validDeck = []
        for currentSuit in range(1, 5):  # valid deck
            for currentRank in range(1, 14):
                card1 = card.Card(currentSuit, currentRank)
                self.validDeck.append(card1)

    # test count for proper full deck of 52.
    def test_full_deck(self):
        expected_response = 52
        self.assertEqual(expected_response, self.actual_response1.count())  # check for full deck

    # Test for proper count of deck if all cards are drawn out of the deck.
    def test_empty_deck(self):

        expected_response = 0  # expected response of empty deck

        for x in range(52):  # empty out deck
            self.actual_response.draw()

        self.assertEqual(expected_response, self.actual_response.count)  # check for empty deck

    # test for count less than 0. if deck is empty and an attempt to draw is issued,
    # an exception is thrown.
    def test_non_positive(self):
        for x in range(52):  # empty out deck and draw more than 52 cards
            self.actual_response1.draw()
        with self.assertRaises(IndexError):
            self.actual_response1.draw()

    # test for count greater than 52.
    def test_init_overstock_deck(self):
        # count should be less than or equal to 52.
        self.assertLessEqual(self.actual_response2.count(), 52)
        # draw a card from the deck
        self.actual_response2.draw()
        # after drawing, the count should be less than or equal to 52.
        self.assertLessEqual(self.actual_response2.count(), 52)

        # Test count of draws from 52 to 0

    def test_init_semi_deck(self):
        expected_response = 52  # expected result

        # initial testing of 52 cards
        self.assertEqual(expected_response, self.actual_response3.count())

        # Fully test the deck for proper count of the deck as
        # cards are drawn from the deck.
        # Each card is individually drawn and individually
        # checked for proper count.
        for x in range(52):
            self.actual_response3.draw()  # draw a card
            expected_response = expected_response - 1  # decrement deck count by 1.
            self.assertEqual(expected_response, self.actual_response3.count())  # check for correct count