from django.test import TestCase
# from unittest import TestCase
from PokerProjectDjango.PokerProject.classes import card
from PokerProjectDjango.PokerProject.classes import deck


class CardInitRaiseValueError(TestCase):
    def test_RaiseValueErrorZero(self):  # test with parameters of zero

        with self.assertRaises(ValueError):
            fakeCard = card.Card(0, 1)  # zero suit
        with self.assertRaises(ValueError):
            fakeCard = card.Card(1, 0)  # testing here with 0's on both suit and rank returns a value error
        with self.assertRaises(ValueError):
            fakeCard = card.Card(-1, 0)
        with self.assertRaises(ValueError):
            fakeCard = card.Card(0, 1)
        with self.assertRaises(ValueError):
            fakeCard = card.Card(0, 0)
        with self.assertRaises(ValueError):
            fakeCard = card.Card(0, -1)

    def test_RaiseValueErrorNegative(self):                     #test with negative parameters
        with self.assertRaises(ValueError):
            fakeCard = card.Card(-1, 1)
        with self.assertRaises(ValueError):
            fakeCard = card.Card(1, -1)  # testing here that inputing negative values returns a value error
        with self.assertRaises(ValueError):
            fakeCard = card.Card(-1, 1)
        with self.assertRaises(ValueError):
            fakeCard = card.Card(-1, -1)

    def test_RaiseValueErrorOverLimit(self):                    #test with parameters that are above the max
        with self.assertRaises(ValueError):
            fakeCard = card.Card(6, 4)
        with self.assertRaises(ValueError):
            fakeCard = card.Card(4, 14)
        with self.assertRaises(ValueError):
            fakeCard = card.Card(5, 6)  # test that inputting suit and card parameters
        with self.assertRaises(ValueError):
            fakeCard = card.Card(3, 14)

    def test_RaiseTypeError(self):
        with self.assertRaises(TypeError):
            fakeCard = card.Card("thingy", 1)  # test with invalid type parameters for a TypeError
        with self.assertRaises(TypeError):
            fakeCard = card.Card(True, 1)
        with self.assertRaises(TypeError):
            fakeCard = card.Card(2, "think")
        with self.assertRaises(TypeError):
            fakeCard = card.Card(2, False)


class CardInitCorrectRankAndSuit(TestCase):
    def setUp(self):
        # Card(suit, rank)
        self.aceClubs = card.Card(1, 1)                         #creating multiple cards to test
        self.twoClubs = card.Card(1, 2)
        self.fourDiamonds = card.Card(4, 4)
        self.kingHearts = card.Card(3, 13)

    def test_GetRank(self):
        self.assertEqual(self.aceClubs.getRank(), 1)            #testing the rank is set correctly for each card
        self.assertEquals(self.twoClubs.getRank(), 2)
        self.assertEqual(self.fourDiamonds.getRank(), 4)
        self.assertEqual(self.kingHearts.getRank(), 13)

    def test_GetSuit(self):
        self.assertEqual(self.aceClubs.getSuit(), 1)            #testing the suit is set correctly for each card
        self.assertEqual(self.twoClubs.getSuit(), 1)
        self.assertEqual(self.fourDiamonds.getSuit(), 4)
        self.assertEqual(self.kingHearts.getSuit(), 3)

    def test_GetSuit(self):
        self.assertEqual(self.aceClubs.getSuit(), 1)            #testing the suit is set correctly for each card
        self.assertEqual(self.twoClubs.getSuit(), 1)
        self.assertEqual(self.fourDiamonds.getSuit(), 4)
        self.assertEqual(self.kingHearts.getSuit(), 3)