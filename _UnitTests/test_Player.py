from django.test import TestCase
# from unittest import TestCase
from unittest import mock
from PokerProjectDjango.PokerProject.classes import card
from PokerProjectDjango.PokerProject.classes import deck
from PokerProjectDjango.PokerProject.classes import player


class PlayerTest(TestCase):

    def setUp(self):
        # set up all your objects in here
        # for example
        # self.testCard1 = card.Card(1,2)
        self.player1= player.Player("Evelyn")
        self.player2 = player.Player("Will")
        self.password1 = "abcd"
        self.password2 = "&^^$^%#%(*&)()+(*)(*%$&$#&^"
        self.password3 = ""
        self.num = 500

    def test_constructor(self):
       # make a bad constructor check that it raises an error
       with self.assertRaises(TypeError):
           invalid_player = player.Player(-1)
       with self.assertRaises(Exception):
           invalid_player = player.Player()

    def test_check_username(self):
        # for all tests make sure to pass in a bad value to check if an error is raised
        self.assertTrue(self.player1.check_username("Evelyn"))
        self.assertFalse(self.player1.check_username("Will"))
        self.assertTrue(self.player2.check_username("Will"))

    def test_set_password(self):
        # for all tests make sure to pass in a bad value to check if an error is raised
        self.player1.set_password(self.password1)
        self.player2.set_password(self.password2)
        self.player2.set_password(self.password3)
        self.assertEquals(self.player2.password, self.password1)
        self.assertEquals(self.player1.password, self.password2)
        self.assertEquals(self.player1.password, self.password3)


    def test_update_money(self):
        # for all tests make sure to pass in a bad value to check if an error is raised
        self.player1.update_money(self.num)
        self.assertEquals(self.player1.money, self.num)
        self.player1.update_money(self.num)
        self.assertEquals(self.player1.money, 1000)
        self.assertEquals(self.player2.money, 0)