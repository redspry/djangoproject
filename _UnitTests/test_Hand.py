from django.test import TestCase
# from unittest import TestCase
from PokerProjectDjango.PokerProject.classes import card
from PokerProjectDjango.PokerProject.classes import hand

class HandTest(TestCase):
    def setUp(self):
        # Example hand with a starting bet of 0
        self.fakeHand = hand.Hand(0)
        self.fakeHand2 = hand.Hand(0)
        self.fakeCard = card.Card(1,1)
        self.card1 = card.Card(1, 2)
        self.card2 = card.Card(1, 3)
        self.card3 = card.Card(1, 4)
        self.card4 = card.Card(1, 5)
        self.card5 = card.Card(1,6)
        pass

    def test_constructor(self, buy_in):
        # Making a bad constructor check that it raises an error
        with self.assertRaises(ValueError):
            fakeHand1 = hand.Hand(-1) #Ensuring negative starting bets are not allowed
        with self.assertRaises(TypeError):
            fakeHand2 = hand.Hand('s') #Ensuring invalid starting bets are not allowed
        pass

    def test_add_card(self):
        # for all tests make sure to pass in a bad value to check if an error is raised
        self.assertEqual(self.fakeHand.bet, 0)

        #Adding one card and making sure it is added properly
        self.fakeHand.add_card(card.Card(1,1))
        self.assertEqual(self.fakeHand.cards[0].getSuit(),1)
        self.assertEqual(self.fakeHand.cards[0].getRank(), 1)

        # Ensuring that one hand cannot have two of the same card
        with self.assertRaises(ValueError):
            self.fakeHand.add_card(self.fakeCard)

        #Ensuring that added cards have valid suits and ranks
        with self.assertRaises(ValueError):
            self.fakeHand.add_card(card.Card(-1,1)) #Negative suit
        with self.assertRaises(ValueError):
            self.fakeHand.add_card(card.Card(1,-1)) #Negative rank
        with self.assertRaises(TypeError):
            self.fakeHand.add_card(card.Card('s',1)) #Invalid suit
        with self.assertRaises(TypeError):
            self.fakeHand.add_card(card.Card(1,'r')) #Invalid rank

        #Adding more cards to get up to 5
        self.fakeHand.add_card(self.card1)
        self.fakeHand.add_card(self.card2)
        self.fakeHand.add_card(self.card3)
        self.fakeHand.add_card(self.card4)

        #Ensuring that a hand cannot have more than 5 cards
        with self.assertRaises(IndexError):
            self.fakeHand.add_card(self.card5)

    def test_calculate_hand_value(self):
        # This method is part of sprint2 has yet to be tested as the user stories will determine the value calculation
        pass

    def test_empty_hand(self):

        #Ensuring that the initial size of the hand is 0
        fakeHand = hand.Hand(0)
        self.assertEqual(len(hand.cards),0)

        #Adding a card and assuring that the size of the hand is not 0
        fakeHand.add_card(self.fakeCard)
        self.assertNotEqual(len(hand.cards),0)

        #Calling empty_hand and ensuring the hand size is 0
        fakeHand.empty_hand()
        self.assertEqual(len(hand.cards),0)

    def test_bet_amount(self, number):
        #Creating a hand with initial bet of 0

        #Calling bet_amount and ensuring that it was added properly
        self.fakeHand2.bet_amount(number)
        self.assertEqual(self.fakeHand2.bet, number)

        #Ensuring a negative bet cannot be called
        with self.assertRaises(ValueError):
            self.fakeHand2.bet_amount(-1)

        #Ensuring an invalid bet cannot be called
        with self.assertRaises(TypeError):
            self.fakeHand2.bet_amount('s')

    def test_hand_toString(self, number):
        # This method has yet to be tested as the user stories will determine the correct toString display
        pass