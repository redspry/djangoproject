(if you use **Typora**: https://typora.io/ you can open and edit this file like a word document)

Here is the breakdown of the PBIs we're doing for the ATDD assignment: 

-  A player account should be instantiable, readable, and writable.
-  test_player.py
    o  Test player.__init__(self, username)
    o  Test player.check_username(self, uname)
    o  Test player.set_password(self, passw)
    o  Test player.update_money(self, num)
-  Administrators should be identifiable.
-  test_admin.py
    o  Test pokeradmin.__init__(self, username, password)
    o  Test pokeradmin.check_username(self, uname)
    o  Test pokeradmin.reset_password(self, passw)
    o  Test pokeradmin.create_new_player(self, username, password)
-  A player should be able to identify all the cards in their hand
-  test_hand.py
    o  Test hand.__init__(self, buy_in)
    o  Test hand.add_card(self, card)
    o  Test hand.empty_hand(self)
    o  Test hand.toString(self, number)
-  A player should be able to see the status of the table (players, fold status, etc.)
-  test_table.py
    o  Test table.__init__(self)4
    o  Test table.add(self, Player, buy_in)
    o  Test table.fold(self, Player, bet)
    o  Test table.player_leaves_table(self, Player)
    o  Test table.game_over(self)
    o  Test table.list_table_members(self)
-  A player should be able to access accurate updates regarding bets
-  test_bets.py
    o  Test table.raise_bet(self, Player, bet)
    o  Test table.call_bet(self, Player, bet)
    o  Test hand.bet_amount(self, number)

Of those, here are how the assignments are broken down:
test_player.py - Ian (me)
test_admin1.py - Will
test_admin2.py - Frank
test_hand.py - CK
test_table.py - Ace
test_bets.py - Evelyn

***note there is a runTests.py file but it may error for this assignment because there is a lot of extended functionality that does not exist yet.***