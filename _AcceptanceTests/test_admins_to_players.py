from django.test import TestCase
# from unittest import TestCase
from PokerProjectDjango.PokerProject.classes.player import Player
from PokerProjectDjango.PokerProject.classes.dealer import Dealer
from PokerProjectDjango.PokerProject.classes.table import Table

class TestUsers(TestCase):
    def setUp(self):
        self.Dealer1 = Dealer('Dealer1', 'Password', True)
        self.Dealer2 = Dealer('Dealer2', 'Password', True)
        self.Dealer1 = Dealer(0000000, 'Password', True)
        self.Dealer2 = Dealer('Dealer3', 0, True)

        self.Player1 = Player('Player1')
        self.Player2 = Player('Player2')
        self.Player3 = Player('Player3')
        self.Player4 = Player('Player4')
        self.Player5 = Player('Player5')
        self.Player6 = Player('Player6')
        self.Player7 = Player('Player7')
        self.Player8 = Player('Player8')
        self.Player9 = Player('Player9')
        self.Player9 = Player('Player10')
        self.Player9 = Player('Player11')

        self.Table1 = Table('Table1')
        self.Table2 = Table('Table2')
        self.Table3 = Table('Table3')

    ##############################################################################
    # PBI#1 - As a Dealer I want to create player accounts by creating a username,
    # so that I can add new players to a table (L))
    # Dealer accounts can be created with a username & password
    ##############################################################################

    def test_validDealerLogin(self):
        # Dealer must successfully login with valid username
        self.assertEqual(self.ui.command("Login Dealer1 Password"), "Dealer1 Logged In")
        self.assertEqual(self.ui.command("Login Dealer1 Password"), "Dealer Already Logged In")
        self.assertEqual(self.ui.command("Login Dealer2 Password"), "Dealer2 Logged In")
        self.assertEqual(self.ui.command("Login Dealer1 Password"), "Dealer Already Logged In")

    def test_invalidDealerLogin(self):
        self.ui.command("Login Dealer1 Password")
        # Dealer must not successfully login with invalid username or password
        # invalid username
        self.assertEqual(self.ui.command("Login Dealer12 Password"), "Invalid dealer login")
        self.assertEqual(self.ui.command("Login DEALER1 Password"), "Invalid dealer login")
        self.assertEqual(self.ui.command("Login DEALer2 Password"), "Invalid dealer login")
        # invalid username whitespace
        self.assertEqual(self.ui.command("Login Dealer2  Password"), "Invalid dealer login")

    def test_invalidDealerPassword(self):
        self.ui.command("Login Dealer1 Password")
        # invalid password whitespace
        self.assertEqual(self.ui.command("Login Dealer1 Password "), "Invalid dealer password")
        # invalid password
        self.assertEqual(self.ui.command("Login Dealer2 Password1"), "Invalid dealer password")
        self.assertEqual(self.ui.command("Login Dealer2 PASSWORD"), "Invalid dealer password")
        self.assertEqual(self.ui.command("Login Dealer1 pASSword"), "Invalid dealer password")

    def test_invalidDealerEntrys(self):
        self.ui.command("Login Dealer1 Password")
        # invalid username syntax
        self.assertEqual(self.ui.command("Login 0000000 Password"), "Invalid dealer login")
        # invalid password syntax
        self.assertEqual(self.ui.command("Login Dealer3 0"), "Invalid dealer login password")

    # Dealer can create player accounts (username only, no passwords)
    def test_validPlayerCreation(self):
            self.ui.command("Login Dealer1 Password")
            self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
            self.assertEqual(self.ui.command("Dealer1: Create Player Player3"), 'the User <Player3> added')
            self.assertEqual(self.ui.command("Dealer1: Create Player Player4"), 'the User <Player4> added')
            self.assertEqual(self.ui.command("Dealer1: Create Player Player5"), 'the User <Player5> added')
            self.assertEqual(self.ui.command("Dealer1: Create Player Player6"), 'the User <Player6> added')
            self.assertEqual(self.ui.command("Dealer1: Create Player Player7"), 'the User <Player7> added')
            self.assertEqual(self.ui.command("Dealer1: Create Player Player8"), 'the User <Player8> added')
            self.assertEqual(self.ui.command("Dealer1: Create Player Player9"), 'the User <Player9> added')
            self.assertEqual(self.ui.command("Dealer1: Create Player Player10"), 'the User <Player10> added')

    def test_invalidPlayerCreation(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.assertEqual(self.ui.command("Dealer1: Create Player Player1"), 'error <Player1> already exists')
        self.assertEqual(self.ui.command("Dealer1: Create Player Player2"), 'error <Player2> already exists')
        #invalid syntax
        self.assertEqual(self.ui.command("Dealer1: Create (Player)_10"), 'Player username is invalid')
        self.assertEqual(self.ui.command("Dealer1: Create (Player "), 'Player username is invalid')
        self.assertEqual(self.ui.command("Dealer1: Create Player "), 'Player username is invalid')

    # Created players must have 100 starting chips
    def test_validCreatedPlayerChipCount(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.assertEqual(self.ui.command("Dealer1: start Table2 Player3,Player4,Player5,Player6"), "Table2 in progress")
        self.assertEqual(self.ui.command("Player1: chip_amount"), 100)
        self.assertEqual(self.ui.command("Player2: chip_amount"), 100)
        self.assertEqual(self.ui.command("Player3: chip_amount"), 100)
        self.assertEqual(self.ui.command("Player4: chip_amount"), 100)
        self.assertEqual(self.ui.command("Player5: chip_amount"), 100)
        self.assertEqual(self.ui.command("Player6: chip_amount"), 100)

    #Dealer can make a new table, Player can not make a new Table
    def test_validDealerTableCreation(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player3,Player4"), "ERROR! Table1 in progress")
        self.assertEqual(self.ui.command("Dealer1: start Table2 Player3,Player4,Player5"), "Table2 in progress")
        self.assertEqual(self.ui.command("Dealer1: start Table2 Player3,Player4,Player5"), "ERROR! Table2 in progress")

    def test_invalidPlayerTableCreation(self):
        self.ui.command("Login Dealer1 Password")
        # Players may not start tables
        self.assertEqual(self.ui.command("Player1: start Table1 Player1,Player2"), "ERROR!")
        self.assertEqual(self.ui.command("Player2: start Table2 Player1,Player2"), "ERROR!")

    # Dealer can create and add up to 10 players to A table, but must add a minimum of 2
    def test_validTableCreation(self):
        self.ui.command("Login Dealer1 Password")
        # must add a min of 2
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.assertEqual(self.ui.command("Dealer1: start Table2 Player3,Player4"), "Table1 in progress")
        self.assertEqual(self.ui.command("Dealer1: start Table3 Player5,Player6"), "Table1 in progress")

    def test_invalidTableCreation(self):
        # min of 2 and less than 10
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1"), "Table1 invalid too few players")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player1"), 'ERROR! the User <Player1> already added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player2"), 'ERROR! the User <Player2> already added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player3"), 'the User <Player3> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player4"), 'the User <Player4> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player5"), 'the User <Player5> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player6"), 'the User <Player6> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player7"), 'the User <Player7> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player8"), 'the User <Player8> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player8"), 'the User <Player9> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player10"), 'the User <Player10> added')
        # Dealer can create and add up to 10 players to a table
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player11"), 'ERROR! the User <Player11> could not be added, table full')

    def test_validAddingPlayers(self):
        # add up to 10 players
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player3"), 'the User <Player3> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player4"), 'the User <Player4> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player5"), 'the User <Player5> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player6"), 'the User <Player6> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player7"), 'the User <Player7> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player8"), 'the User <Player8> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player8"), 'the User <Player9> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player10"), 'the User <Player10> added')

    def test_invalidAddingPlayers(self):
        # add up to 10 players
        self.ui.command("Login Dealer1 Password")
        # add too few
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1"), "Table1 invalid too few players")
        #add too many #incorrect syntax
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1"), "Table1 invalid too few players")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player1"), 'ERROR! the User <Player1> already added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player2"), 'ERROR! the User <Player2> already added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player3"), 'the User <Player3> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player4"), 'the User <Player4> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player5"), 'the User <Player5> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player6"), 'the User <Player6> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player7"), 'the User <Player7> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player8"), 'the User <Player8> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player8"), 'the User <Player9> added')
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player10"), 'the User <Player10> added')
        # Dealer can create and add up to 10 players to a table
        self.assertEqual(self.ui.command("Dealer1: Table1 Add Player Player11"), 'ERROR! the User <Player11> could not be added, table full')
        # incorrect syntax
        self.assertEqual(self.ui.command("Dealer1: start Table2 Player3,Player4,Player5"), "Table1 in progress")
        self.assertEqual(self.ui.command("Dealer2: Table2 Add Player Dealer2"), 'ERROR! the Dealer2 could not be added')