from django.test import TestCase
# from unittest import TestCase
from PokerProjectDjango.PokerProject.classes.player import Player
from PokerProjectDjango.PokerProject.classes.dealer import Dealer
from PokerProjectDjango.PokerProject.classes.table import Table

class TestTable(TestCase):
    def setUp(self):
        self.Dealer1 = Dealer('Dealer1', 'Password', True)

        self.Player1 = Player('Player1')
        self.Player2 = Player('Player2')
        self.Player3 = Player('Player3')

        self.Table1 = Table('Table1')

    ##############################################################################
    # PBI#4 — As a player at a table during a hand, I can ask to see my cards if the hand has been dealt,
    # so I can decide how to bet (M)

    # Acceptance Criteria:
    # Each response should include that players total chips,
    # that player's current bet, and status (call, raise, check, fold).
    # Maximum raise is 5 chips
    ##############################################################################

    # A hand is dealt as soon as two players are at a table.
    def test_validHandDealt(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        # hand is randomly generated
        self.assertEqual(self.ui.command("Player1: show hand"),
                         "Player1@Table1: CHIPS:100 STATUS:none BET:0 HAND:" + self.Player1.show_hand() + " NUM_CARDS:2")
        self.assertEqual(self.ui.command("Player2: show hand"),
                         "Player2@Table1: CHIPS:100 STATUS:none BET:0 HAND:" + self.Player1.show_hand() + " NUM_CARDS:2")

    # A new user must wait until the current hand is complete before being dealt in.
    def test_invalidHandInProgress(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        # hand is randomly generated
        self.assertEqual(self.ui.command("Player1: show hand"),
                         "Player1@Table1: CHIPS:100 STATUS:none BET:0 HAND:" + self.Player1.show_hand() + " NUM_CARDS:2")
        self.assertEqual(self.ui.command("Player2: show hand"),
                         "Player2@Table1: CHIPS:100 STATUS:none BET:0 HAND:" + self.Player1.show_hand() + " NUM_CARDS:2")

        self.assertEqual(self.ui.command("Dealer1: Create Player Player3"), 'the User <Player1> added')
        self.assertEqual(self.ui.command("Dealer1: Create Player Player4"), 'the User <Player2> added')

        self.assertEqual(self.ui.command("Player3: show hand"),"no current hand game in progress...")
        self.assertEqual(self.ui.command("Player4: show hand"),"no current hand game in progress...")

    # Betting for 4 “streets” until game over
    def test_validBetToFiveCardsInHand(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")

        # hand is randomly generated
        self.assertEqual(self.ui.command("Player1: show hand"),
                         "Player1@Table1: CHIPS:100 STATUS:none BET:0 HAND:" + self.Player1.show_hand() + " NUM_CARDS:2")
        self.assertEqual(self.ui.command("Player2: show hand"),
                         "Player2@Table1: CHIPS:100 STATUS:none BET:0 HAND:" + self.Player2.show_hand() + " NUM_CARDS:2")

        # STREET 1 # Next card is automatically dealt after last player at the table finishes a round
        self.assertEqual(self.ui.command("Player1: bet 5"),
                         "Player1@Table1: CHIPS:95 STATUS:bet BET:5 HAND:" + self.Player1.show_hand() + " NUM_CARDS:2")
        self.assertEqual(self.ui.command("Player2: call 5"),
                         "Player2@Table1: CHIPS:95 STATUS:call BET:5 HAND:" + self.Player2.show_hand() + " NUM_CARDS:3")
        # NOTE: Next card is automatically dealt after last player at the table finishes a round
        self.assertEqual(self.ui.command("Player1: show hand"),
                         "Player1@Table1: CHIPS:95 STATUS:bet BET:5 HAND:" + self.Player1.show_hand() + " NUM_CARDS:3")
        # STREET 2
        self.assertEqual(self.ui.command("Player1: bet 5"),
                         "Player1@Table1: CHIPS:90 STATUS:bet BET:10 HAND:" + self.Player1.show_hand() + " NUM_CARDS:3")
        self.assertEqual(self.ui.command("Player2: call 5"),
                         "Player2@Table1: CHIPS:90 STATUS:call BET:10 HAND:" + self.Player2.show_hand() + " NUM_CARDS:4")
        self.assertEqual(self.ui.command("Player1: bet 5"),
                         "Player1@Table1: CHIPS:85 STATUS:bet BET:15 HAND:" + self.Player1.show_hand() + " NUM_CARDS:4")
        self.assertEqual(self.ui.command("Player2: call 5"),
                         "Player2@Table1: CHIPS:85 STATUS:call BET:15 HAND:" + self.Player2.show_hand() + " NUM_CARDS:5")
        # STREET 4 - LAST BETS WITH 5 CARDS
        self.assertEqual(self.ui.command("Player1: bet 5"),
                         "Player1@Table1: CHIPS:80 STATUS:bet BET:20 HAND:" + self.Player1.show_hand() + " NUM_CARDS:5")
        self.assertEqual(self.ui.command("Player2: call 5"),
                         "Player2@Table1: CHIPS:80 STATUS:call BET:20 HAND:" + self.Player2.show_hand() + " NUM_CARDS:5")

    # Once everyone folds or calls, the contents of any non-folded hands are revealed,
    # and chip totals are updated.
    def test_validGameOver(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.ui.command("Player1: bet 4")
        self.ui.command("Player2: bet 5")
        self.ui.command("Player1: bet 4")
        self.ui.command("Player2: bet 5")
        self.ui.command("Player1: bet 4")
        self.ui.command("Player2: bet 5")
        self.ui.command("Player1: bet 4")
        self.ui.command("Player2: bet 5")

        self.assertEqual(self.ui.command("Player1: show hand"),
                         "Player1@Table1: CHIPS:84 STATUS:bet BET:16 HAND:" + self.Player1.show_hand() + " NUM_CARDS:5")
        self.assertEqual(self.ui.command("Player2: show hand"),
                         "Player2@Table1: CHIPS:80 STATUS:call BET:20 HAND:" + self.Player2.show_hand() + " NUM_CARDS:5")

        self.assertEqual(self.ui.command("Player1: show table status"),
                         "GAME OVER!: Pot:36 Hands:" + "Player1:" + self.Player1.show_hand() + "," + "Player1:" + self.Player2.show_hand())


    # create table, of two players run a round, at the end reveal hands of players
    def test_validGameOverChipsUpdated(self):
        self.ui.command("Login Dealer1 Password")
        self.assertEqual(self.ui.command("Dealer1: start Table1 Player1,Player2"), "Table1 in progress")
        self.ui.command("Player1: bet 4")
        self.ui.command("Player2: bet 5")
        self.ui.command("Player1: bet 4")
        self.ui.command("Player2: bet 5")
        self.ui.command("Player1: bet 4")
        self.ui.command("Player2: fold")

        self.assertEqual(self.ui.command("Player1: show hand"),
                         "Player1@Table1: CHIPS:88 STATUS:bet BET:12 HAND:" + self.Player1.show_hand() + " NUM_CARDS:5")
        self.assertEqual(self.ui.command("Player2: show hand"),
                         "Player2@Table1: CHIPS:90 STATUS:fold BET:10 HAND:" + self.Player2.show_hand() + " NUM_CARDS:5")

        self.assertEqual(self.ui.command("Player2: show table status"),
                         "GAME OVER!: Pot:22 Hands:" + "Player1:" + self.Player1.show_hand() + "," + "Player1:" + self.Player2.show_hand())