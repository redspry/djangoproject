[TOC]

## i. UNIT TESTS: How to run them

I wrote some code in the project to run the tests in Pycharm.** 

> How to use it to Run and Test your Unit Tests Locally:
>
> **go to the folder _UnitTests -> _UnitTests/runTests.py** 
>
> in pyCharm right click on **runTests.py**  and select "run"

**Unit Tests Reference**

https://docs.python.org/3.4/library/unittest.html#assert-methods

------



## ii. UNIT TESTS: who is doing what + instructions

> **for all of them we are missing...**
>
> **all objects made in a setUp() method**, also missing a test to make the class badly, with the wrong values etc. **assertRaises()** when we make a card or a deck with bad values, or a bad class. 
>
> examples:
>
> 1. **class WidgetTestCase(unittest.TestCase): using setUp()** to make your objects before you test them from https://docs.python.org/3/library/unittest.html      
> 2. **def method(self):  how to test an exception is thrown when you do something illegal** for a bad constructor, add a method like this. Do something illegal and make sure you get an error and it doesn't let you. **do_something()** would be i.e. try to make Card.card(1,-1), or do something illegal and make sure you get an error and it doesn't let you do the illegal thing https://docs.python.org/3/library/unittest.html && https://kite.com/python/docs/unittest.TestCase.assertRaises

```python
---------------------------------------------
  
def method(self):
		with self.assertRaises(SomeException):
  			do_something()
          
---------------------------------------------
```

```python
---------------------------------------------
  
class WidgetTestCase(unittest.TestCase):

    def setUp(self):
        self.widget = Widget('The widget')

    def test_default_widget_size(self):
        self.assertEqual(self.widget.size(), 50)
          
--------------------------------------------- 
```

*i.e. size() method should return the number 50

------



1. **_UnitTests/test_DeckDraw: Josh**
2. **_UnitTests/test_CardGet: Ian** 
3. **Evelyn  _UnitTests/test_CardStr & test_Player**   (not much to do for the first one, put objects to create in a setUp method see DeckInit), the second you will have to write, you can look at others for rerefence there is a template 
4. **Will   _UnitTests/test_CardInit & test_PokerAdmin** there are some instructions in the class SetUp, and make all objects the other methods use there. I think to make them accessible to other methods you nered to call it self.objectName. The second you will have to write, you can look at others for rerefence there is a template, it should be pretty short. 
5. **_UnitTests/test_DeckInit: Ace** this one is kind of messy, very, I think there might be some issues with the mock deck. I can help with this one.
6. **_UnitTests/test_Hand: Frank** you will have to write, you can look at other tests for rerefence, there is a template, but this one might be tricky. 



## iii. UNIT TESTS: How can I tell if they are passing?

when you run the unit tests using step (i), you're going to see a lot of output. 

```python

----------------------------------------------------------------------
Ran 34 tests in 0.023s

FAILED (failures=8, errors=3)
<unittest.runner.TextTestResult run=34 errors=3 failures=8>
********************
```

scroll up until you see this line... note failures are ok for our tests, depending on how they are written, the classes don;t exist yet, but we want to avoid errors. Errors mean the code is bad somewhere. Try to scroll up to find your test and see what line your error is on. 



## help my Pycharm Liscense Expired?

did your pycharm license expire? get a student license with this link, click the "apply now" part

https://www.jetbrains.com/community/education/#students



## Basic Git Commands

**when you start editing code...**

open up  **terminal** if you're in mac, or **command prompt** or **git bash** if you're in windows

use these two commands to get to where you want to put your repo, 

```python
ls # this will just diaplay all files in the directory you are in
```

```python
cd WhereverYouWantToPutYourRepo.../Documents/git  # type in the file location where you cloned your repo into
# pro tip, type in the first letter then hitting TAB will fill in the rest
```

**cool hack for windows,** just make a normal folder, open it, then right click inside of the empty folder and choose "**git bash here**" or **"command prompt here"**

#### **STEPS**

1. clone the repo with the argument, this will make a new folder in a directory (JUST COPY FROM HERE ^ IN BIT_BUCKET)

   ```python
   git clone https://USERNAME@bitbucket.org/USERNAME/red_ryders.git
   ```

2. checkout a branch and make your edits in the new branch with

   ```python
   git checkout -b random_name_you_choose_for_your_branch**
   ```

3. after you make changes and want to add them to the repo there are three steps

   ```python
   git add .
   ```

   ```python
   git commit -m "add your message"
   ```

   ```python
   git push origin random_name_you_choose_for_your_branch
   ```

4. you can also just use the pycharm IDE buttons

**note, again, please don't push directly to master**

------

##### WARNING!: DO NOT PUSH DIRECTLY TO MASTER! ....... CHECKOUT A BRANCH AFTER CLONING BY FOLLOWING THESE STEPS

```python
- git checkout -b random_name_you_choose_for_your_branch
```

push back to the repo all you want, **when you are done with your branch, it can be merged back into master.**

 http://gitignore.io/ with django,pycharm+all generates a git ignore file with syntax to ignore user specific directories or log files that should not be pushed.

------

**Files still pushing that you don't want? WHY IS GITIGNORE NOT UPDATING WHEN I ADDED MY FILENAME?**

- for example **I pushed a .idea** folder, yikes, **how do I fix that**? 

  ```
  "git rm --cached -r .idea" 
  ```
  
  ```
  if git is not ignoring something try running:
  "git rm --cached -r filename" 
```
  
  ```
  check what the git ignore file is ignoring with 
  "git status --ignored"
  ```
  
  this will update the tracked ignored files, and remove something you previously pushed. It won't delete it locally, but it won't add it to the shared repo 



## BitBucket Tutorial By BitBucket

------

Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

### Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

### Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

### Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).