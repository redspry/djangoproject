from django.db import models

#######################################
# This class is not done and is a WIP
########################################

class Dealer(models.Model):
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    is_online = models.BooleanField(default=False)

    def __str__(self):
        return self.username


class Player(models.Model):
    # ...
    username = models.CharField(max_length=20)
    money = models.IntegerField(default=0)
    def __str__(self):
        return self.username


class Hand(models.Model):
    # this ties a player to a hand
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    bet = models.IntegerField(default=0)
    def __str__(self):
        return self.cards


class Game(models.Model):
    rounds = models.IntegerField(default=0)
    pot = models.IntegerField(default=0)
    folds = models.CharField(max_length=300)
    winner = models.CharField(max_length=20)
    def __str__(self):
        return self.winner


class Table(models.Model):
    num_players = models.IntegerField(default=0)
    buy_in = models.IntegerField(default=0)
    # this ties a dealer to a table
    dealer = models.ForeignKey(Dealer, on_delete=models.CASCADE)
    # # this ties players to a table
    # players = models.ForeignKey(Player, on_delete=models.CASCADE)
    # this ties a game to a table
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
