from PokerProjectDjango.PokerProject.classes import card
from PokerProjectDjango.PokerProject.classes import deck
from PokerProjectDjango.PokerProject.classes import player as Player
from PokerProjectDjango.PokerProject.classes import dealer
from PokerProjectDjango.PokerProject.classes import hand

class Table:
    # The table class is a class to manage a dictionary of players and cards.
    def __init__(self, TableName):
        self.tableName = TableName
        self.currentBet = 0
        self.pot = 0
        self.capacity = 10
        self.players = []

    # add a new player to the table
    def add(self, Player, buy_in):
        self.players.append(Player)
        self.pot = self.pot + buy_in
        # empty hand with the buy in amount, add the player
        pass

    def raise_bet(self, Player, bet):
        Player.money = Player.money - bet
        self.currentBet = self.currentBet + bet
        self.pot = self.pot + bet
        # get hand object using player as a key
        # Update hand bet ammount with hand.bet_amount(bet)
        # save it in the dictionary
        pass

    def call_bet(self, Player, bet):
        Player.money - bet
        self.pot = self.pot + bet
        # subtract the player bet
        # get hand object using player as a key
        # Update hand bet ammount with hand.bet_amount(bet)
        # save it in the dictionary
        # check if the last player
        pass

    def fold(self, Player, bet):
        self.currentBet = 0
        # get hand object using player as a key
        # remove hand from the tabel dictionary
        pass

    def player_leaves_table(self, Player):
        self.currentBet = 0
        # get player object from table dictionary
        # delete player and hand
        pass

    def game_over(self):
        self.currentBet = 0
        self.pot = 0
        # clear all hands from the list of players
        pass

    def list_table_members(self):
        # for player, hand in self.tableitems():
        #     print("Name: " + player)
        #     print("Age: " + str(hand) + "\n")
            # will need to use a hand to string method
        pass


