from PokerProjectDjango.PokerProject.classes import card
from PokerProjectDjango.PokerProject.classes import deck
from PokerProjectDjango.PokerProject.classes import hand


class Player:
  def __init__(self, username):
      self.username = username
      self.money = 100 # new players start with 100 chips.

  def update_money(self, chips):
      self.money = self.money + chips
      pass

  def check_username(self, uname):
    if self.username == uname:
        pass

  def player_check(self):
      pass

  def player_bet(self):
      # this player can choose to bet
      pass

  def player_raise(self):
      # this player can choose to raise his/her hand
      pass

  def player_fold(self):
      # this player can choose to fold
      pass

  def get_username(self):
      # return the username of this player
      pass

  def get_coins(self):
      # return the amount of coins this player has
      pass

  def new_game(self):
      # this player can choose to start a different/new game
      # but can't deal or start the actual game
      pass

  def show_hand(self):
      # this will show the current hand that the player has
      pass

  def num_cards_in_hand(self):
      # this will show the current hand that the player has
      pass