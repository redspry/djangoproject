from django.shortcuts import render
from django.views import View
from PokerProjectDjango.PokerProject.models import MyModel

# Create your views here.
class Home(View):
  def get(self,request):
    return render(request, 'main/index.html')
  def post(self,request):
    yourInstance = MyModel()
    commandInput = request.POST["command"]
    userName = request.POST["user"]
    if commandInput:
      response = yourInstance.command(userName+":"+commandInput)
    else:
      response = ""
    return render(request, 'main/index.html',{"message":response})